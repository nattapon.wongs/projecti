
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, redirect, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config["DEBUG"] = True
SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="nattaponwongs",
    password="Pp150343",
    hostname="nattaponwongs.mysql.pythonanywhere-services.com",
    databasename="nattaponwongs$product2",
)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Comment(db.Model):

    __tablename__ = "product"

    id = db.Column(db.Integer, primary_key=True)
    productId = db.Column(db.String(15))
    productAmount = db.Column(db.String(4096))
    productName = db.Column(db.String(4096))
    date = db.Column(db.String(4096))
    content = db.Column(db.String(4096))

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        return render_template("main_page.html", product=Comment.query.all())

    comment = Comment(content=request.form["contents"],productId=request.form["productIds"],productAmount=request.form["productAmounts"],productName=request.form["productNames"],date=request.form["dates"])

    db.session.add(comment)
    db.session.commit()
    return redirect(url_for('index'))
